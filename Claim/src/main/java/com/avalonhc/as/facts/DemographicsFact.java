package com.avalonhc.as.facts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.logging.Logger; 



/**
 * <p>Java class for DemographicsFact complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DemographicsFact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="procedureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthPlanId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromDateOfService" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="associatedMPolicy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="associatedMnc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodesList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calculatedAge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="patientDOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="primaryDiagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientGenderCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

@XmlType(name = "DemographicsFact", propOrder = {
    "procedureCode",
    "healthPlanId",
    "fromDateOfService",
    "associatedMPolicy",
    "associatedMnc",
    "diagnosisCodesList",
    "decision",
	"calculatedAge",
	"patientDOB",
	"primaryDiagnosisCode",
	"patientGenderCode",
	"ListOfSecondaryCodes"
	

})
@XmlRootElement(name = "DemographicsFact")

public class DemographicsFact implements java.io.Serializable
{
	private final static Logger LOGGER = Logger.getLogger(DemographicsFact.class.getName());  

   static final long serialVersionUID = 1L;
   private java.lang.String procedureCode;
   private java.lang.String healthPlanId;
   private java.util.Date fromDateOfService;
   private java.lang.String decision;
   private java.lang.String associatedMPolicy;
   private java.lang.String associatedMnc;
   private List<String> diagnosisCodesList = new ArrayList<String>();
   private ArrayList<SecondaryCodes> ListOfSecondaryCodes = new ArrayList<SecondaryCodes>();
   private int calculatedAge;
   private String patientDOB;
   private String primaryDiagnosisCode;
   private String patientGenderCode;
   
   
public java.lang.String getProcedureCode() {
	return procedureCode;
}
public void setProcedureCode(java.lang.String procedureCode) {
	this.procedureCode = procedureCode;
}
public java.lang.String getHealthPlanId() {
	return healthPlanId;
}
public void setHealthPlanId(java.lang.String healthPlanId) {
	this.healthPlanId = healthPlanId;
}
public java.util.Date getFromDateOfService() {
	return fromDateOfService;
}
public void setFromDateOfService(java.util.Date fromDateOfService) {
	this.fromDateOfService = fromDateOfService;
}
public java.lang.String getDecision() {
	return decision;
}
public void setDecision(java.lang.String decision) {
	this.decision = decision;
}
public java.lang.String getAssociatedMPolicy() {
	return associatedMPolicy;
}
public void setAssociatedMPolicy(java.lang.String associatedMPolicy) {
	this.associatedMPolicy = associatedMPolicy;
}
public java.lang.String getAssociatedMnc() {
	return associatedMnc;
}
public void setAssociatedMnc(java.lang.String associatedMnc) {
	this.associatedMnc = associatedMnc;
}
public List<String> getDiagnosisCodesList() {
	return diagnosisCodesList;
}
public void setDiagnosisCodesList(List<String> diagnosisCodesList) {
	this.diagnosisCodesList = diagnosisCodesList;
}
public int getCalculatedAge() {
	return calculatedAge;
}
public void setCalculatedAge(int calculatedAge) {
	this.calculatedAge = calculatedAge;
}
public String getPatientDOB() {
	return patientDOB;
}
public void setPatientDOB(String patientDOB) {
	this.patientDOB = patientDOB;
}
public String getPrimaryDiagnosisCode() {
	return primaryDiagnosisCode;
}
public void setPrimaryDiagnosisCode(String primaryDiagnosisCode) {
	this.primaryDiagnosisCode = primaryDiagnosisCode;
}
public String getPatientGenderCode() {
	return patientGenderCode;
}
public void setPatientGenderCode(String patientGenderCode) {
	this.patientGenderCode = patientGenderCode;
}
   
      
public ArrayList<SecondaryCodes> getListOfSecondaryCodes() {
	return ListOfSecondaryCodes;
}
public void setListOfSecondaryCodes(
		ArrayList<SecondaryCodes> listOfSecondaryCodes) {
	ListOfSecondaryCodes = listOfSecondaryCodes;
}
public boolean isAgeBetween(int calculatedAge,int frmAge, int tAge){
	   boolean flag=false;
	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge);
	   if(calculatedAge > frmAge && calculatedAge < tAge){
		   flag = true;
	   }
	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge+ "flag" + flag);
	   return flag;
}

public boolean isSerivceLineDiagonsisCodesIn(List<String> diagnosisCodeLists,String rulesParam){
	   LOGGER.info("Start of sublistMatchCode method calling" );
	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
	   boolean flag = false;
	   Iterator<String> itr = diagnosisCodeLists.iterator();
	      while(itr.hasNext()) {
	          String diagnosisCode = itr.next();
	          if(rulesList.contains(diagnosisCode)){
	        	  flag = true;

	          }
	      }
	   LOGGER.info("ruleslist" +rulesList+"  diagnosisCodeLists" +diagnosisCodeLists);
	   LOGGER.info("flag-->" +flag);
	   return flag;
}
     
}
