package com.avalonhc.as.facts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.logging.Logger; 



/**
 * <p>Java class for PxDxMatchFact complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PxDxMatchFact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="procedureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthPlanId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromDateOfService" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DiagnosisCodes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="associatedMPolicy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="associatedMnc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodesList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientDOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodePointers" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dxCanHaveSecondery" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodesLists" type="{http://wro1.avalonhc.jbpmremote}DiagnosisCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="seconddiagnosisCodesLists" type="{http://wro1.avalonhc.jbpmremote}DiagnosisCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="diagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secondaryDiagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PxDxMatchFact", propOrder = {
    "procedureCode",
    "healthPlanId",
    "fromDateOfService",
    "primaryDiagnosisCode",
    "DiagnosisCodes",
    "error",
    "associatedMPolicy",
    "associatedMnc",
    "diagnosisCodesList",
    "decision",
	"diagnosisCodePointers",
	"dxCanHaveSecondery",
	"diagnosisCodesLists",
	"seconddiagnosisCodesLists",
	"diagnosisCode",
    "secondaryDiagnosisCode",
    "ListOfSecondaryCodes"

})
@XmlRootElement(name = "PxDxMatchFact")

public class PxDxMatchFact implements java.io.Serializable
{
	private final static Logger LOGGER = Logger.getLogger(PxDxMatchFact.class.getName());  

   static final long serialVersionUID = 1L;
    private java.lang.String procedureCode;
   private java.lang.String healthPlanId;
   private java.util.Date fromDateOfService;
   private String primaryDiagnosisCode;
   private String DiagnosisCodes;
   private String error;
   private java.lang.String decision;
   private java.lang.String associatedMPolicy;
   private java.lang.String associatedMnc;
   private List<String> diagnosisCodesList = new ArrayList<String>();
   private Integer diagnosisCodePointers;
   private String dxCanHaveSecondery;
   private ArrayList<DiagnosisCode> diagnosisCodesLists = new ArrayList<DiagnosisCode>();
   private List<String> seconddiagnosisCodesLists = new ArrayList<String>();
   private String diagnosisCode;
   private String secondaryDiagnosisCode;
   private ArrayList<SecondaryCodes> ListOfSecondaryCodes = new ArrayList<SecondaryCodes>();
  
   	

public java.lang.String getProcedureCode() {
	return procedureCode;
}

public void setProcedureCode(java.lang.String procedureCode) {
	this.procedureCode = procedureCode;
}

public java.lang.String getHealthPlanId() {
	return healthPlanId;
}

public void setHealthPlanId(java.lang.String healthPlanId) {
	this.healthPlanId = healthPlanId;
}

public java.util.Date getFromDateOfService() {
	return fromDateOfService;
}

public void setFromDateOfService(java.util.Date fromDateOfService) {
	this.fromDateOfService = fromDateOfService;
}

public String getPrimaryDiagnosisCode() {
	return primaryDiagnosisCode;
}

public void setPrimaryDiagnosisCode(String primaryDiagnosisCode) {
	this.primaryDiagnosisCode = primaryDiagnosisCode;
}

public String getDiagnosisCodes() {
	return DiagnosisCodes;
}

public void setDiagnosisCodes(String diagnosisCodes) {
	DiagnosisCodes = diagnosisCodes;
}

public String getError() {
	return error;
}

public void setError(String error) {
	this.error = error;
}

public java.lang.String getDecision() {
	return decision;
}

public void setDecision(java.lang.String decision) {
	this.decision = decision;
}

public java.lang.String getAssociatedMPolicy() {
	return associatedMPolicy;
}

public void setAssociatedMPolicy(java.lang.String associatedMPolicy) {
	this.associatedMPolicy = associatedMPolicy;
}

public java.lang.String getAssociatedMnc() {
	return associatedMnc;
}

public void setAssociatedMnc(java.lang.String associatedMnc) {
	this.associatedMnc = associatedMnc;
}

public List<String> getDiagnosisCodesList() {
	return diagnosisCodesList;
}

public void setDiagnosisCodesList(List<String> diagnosisCodesList) {
	this.diagnosisCodesList = diagnosisCodesList;
}

public Integer getDiagnosisCodePointers() {
	return diagnosisCodePointers;
}

public void setDiagnosisCodePointers(Integer diagnosisCodePointers) {
	this.diagnosisCodePointers = diagnosisCodePointers;
}

public String getDxCanHaveSecondery() {
	return dxCanHaveSecondery;
}

public void setDxCanHaveSecondery(String dxCanHaveSecondery) {
	this.dxCanHaveSecondery = dxCanHaveSecondery;
}

public ArrayList<DiagnosisCode> getDiagnosisCodesLists() {
	return diagnosisCodesLists;
}

public void setDiagnosisCodesLists(ArrayList<DiagnosisCode> diagnosisCodesLists) {
	this.diagnosisCodesLists = diagnosisCodesLists;
}

public List<String> getSeconddiagnosisCodesLists() {
	return seconddiagnosisCodesLists;
}

public void setSeconddiagnosisCodesLists(List<String> seconddiagnosisCodesLists) {
	this.seconddiagnosisCodesLists = seconddiagnosisCodesLists;
}

public String getDiagnosisCode() {
	return diagnosisCode;
}

public void setDiagnosisCode(String diagnosisCode) {
	this.diagnosisCode = diagnosisCode;
}

public String getSecondaryDiagnosisCode() {
	return secondaryDiagnosisCode;
}

public void setSecondaryDiagnosisCode(String secondaryDiagnosisCode) {
	this.secondaryDiagnosisCode = secondaryDiagnosisCode;
}



public ArrayList<SecondaryCodes> getListOfSecondaryCodes() {
	return ListOfSecondaryCodes;
}

public void setListOfSecondaryCodes(
		ArrayList<SecondaryCodes> listOfSecondaryCodes) {
	ListOfSecondaryCodes = listOfSecondaryCodes;
}

public boolean isSerivceLineDiagonsisCodesNotIn(List<String> diagnosisCodeLists,String rulesParam){
	   LOGGER.info("Start of sublistCode method calling" );
	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
	   boolean flag = true;
	   Iterator<String> itr = diagnosisCodeLists.iterator();
	      while(itr.hasNext()) {
	          String diagnosisCode = itr.next();
	          if(rulesList.contains(diagnosisCode)){
	        	  flag = false;

	          }

	      }
	   LOGGER.info("ruleslist" +rulesList);

	   return flag;
   }

   public boolean isSerivceLineDiagonsisCodesIn(List<String> diagnosisCodeLists,String rulesParam){
	   LOGGER.info("Start of sublistMatchCode method calling" );
	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
	   boolean flag = false;
	   Iterator<String> itr = diagnosisCodeLists.iterator();
	      while(itr.hasNext()) {
	          String diagnosisCode = itr.next();
	          if(rulesList.contains(diagnosisCode)){
	        	  flag = true;

	          }
	      }
	   LOGGER.info("ruleslist" +rulesList+"  diagnosisCodeLists" +diagnosisCodeLists);
	   LOGGER.info("flag-->" +flag);
	   return flag;
   }

   public Map buildunitsMap(Map unitMap){

	   return unitMap;
   }

   public boolean isAgeBetween(int calculatedAge,int frmAge, int tAge){
	   boolean flag=false;
	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge);
	   if(calculatedAge > frmAge && calculatedAge < tAge){
		   flag = true;
	   }
	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge+ "flag" + flag);
	   return flag;
   }

   public boolean isUnitDayswithinRange(Map<Integer, Integer> claimMap,int units,int days){
	   boolean flag = true;
	   if(claimMap.containsKey(days))
	   {
		   if(claimMap.get(days) > units)
		   {
			   flag = true;
		   }
	   }
	   return flag;
   }

   public boolean isPrimaryDCodeIn(String pDcode,String rulesParam){
	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
	   boolean flag = false;
	   if(rulesList.contains(pDcode)){
    	  flag = true;
	   }
	   return flag;
   }
   

}