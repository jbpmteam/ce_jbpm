package com.avalonhc.as.facts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.logging.Logger; 



/**
 * <p>Java class for ClaimRequest complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ClaimRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="procedureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthPlanId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromDateOfService" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="effectiveEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="primaryDiagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiagnosisCodes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calculatedAge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="associatedMPolicy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="associatedMnc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="primaryProcedureCodeRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodesList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="primaryProcedureCodes" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientDOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthPlanGroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="renderingProviderNpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billingProviderNpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lineOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="blueCardCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inNetworkIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="primaryDecisionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="adviceDecisionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payAndEducate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otherClaimEditorIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodePointers" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dxCanHaveSecondery" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodesLists" type="{http://wro1.avalonhc.jbpmremote}DiagnosisCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="seconddiagnosisCodesLists" type="{http://wro1.avalonhc.jbpmremote}DiagnosisCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="diagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="units" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberOfDays" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="historyRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="secondaryDiagnosisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="patientGenderCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="placeOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paidDeniedIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uniqueMemberId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bussinessSectorCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimRequest", propOrder = {
    "procedureCode",
    "healthPlanId",
    "fromDateOfService",
    "effectiveEndDate",
    "primaryDiagnosisCode",
    "DiagnosisCodes",
    "error",
    "calculatedAge",
    "associatedMPolicy",
    "associatedMnc",
    "primaryProcedureCodeRequired",
    "diagnosisCodesList",
    "primaryProcedureCodes",
    "decision",
    "patientDOB",
    "healthPlanGroupId",
    "renderingProviderNpi",
    "billingProviderNpi",
    "lineOfBusiness",
    "blueCardCode",
    "idCardNumber",
    "inNetworkIndicator",
    "primaryDecisionCode",
    "adviceDecisionType",
    "payAndEducate",
    "otherClaimEditorIndicator",
    "diagnosisCodePointers",
    "dxCanHaveSecondery",
    "diagnosisCodesLists",
    "seconddiagnosisCodesLists",
    "patientGenderCode",
    "placeOfService",
    "diagnosisCode",
     "units",
    "numberOfDays",
    "historyRequired",
    "secondaryDiagnosisCode",
    "rank",
    "paidDeniedIndicator",
    "uniqueMemberId",
    "bussinessSectorCd",
	"excludeMembershipIndicator",
	"cobIndicator"

})
@XmlRootElement(name = "claimRequest")

public class ClaimRequest implements java.io.Serializable
{
	private final static Logger LOGGER = Logger.getLogger(ClaimRequest.class.getName());  

   static final long serialVersionUID = 1L;
   private java.lang.String procedureCode;
   private java.lang.String healthPlanId;
   private java.util.Date fromDateOfService;
   private java.util.Date effectiveEndDate;
   private String primaryDiagnosisCode;
   private String DiagnosisCodes;
   private String error;
   private int calculatedAge;
   private java.lang.String associatedMPolicy;
   private java.lang.String associatedMnc;
   private java.lang.String primaryProcedureCodeRequired;
   private List<String> diagnosisCodesList = new ArrayList<String>();
   private List<Integer> primaryProcedureCodes = new ArrayList<Integer>();
   private java.lang.String decision;
   private String patientDOB;
   private java.lang.String healthPlanGroupId;
   private java.lang.String renderingProviderNpi;
   private String billingProviderNpi;
   private String lineOfBusiness;
   private String blueCardCode;
   private String idCardNumber;
   private java.lang.String inNetworkIndicator;
   private String primaryDecisionCode;
   private String adviceDecisionType;
   private String payAndEducate;
   private java.lang.String otherClaimEditorIndicator;
   private Integer diagnosisCodePointers;
   private String dxCanHaveSecondery;
   private ArrayList<DiagnosisCode> diagnosisCodesLists = new ArrayList<DiagnosisCode>();
   private List<String> seconddiagnosisCodesLists = new ArrayList<String>();
   private String patientGenderCode;
   private String placeOfService;
   private String diagnosisCode;
   private int units;
   private int numberOfDays;
   private boolean historyRequired;
   private int rank;
   private String secondaryDiagnosisCode;
   private String paidDeniedIndicator;
   protected String uniqueMemberId;
   private String bussinessSectorCd;
   private String excludeMembershipIndicator;
   private String cobIndicator;
   
   public String getCobIndicator() {
		return cobIndicator;
	}

	public void setCobIndicator(String cobIndicator) {
		this.cobIndicator = cobIndicator;
	}

   public String getExcludeMembershipIndicator() {
	return excludeMembershipIndicator;
  }

  public void setExcludeMembershipIndicator(String excludeMembershipIndicator) {
	this.excludeMembershipIndicator = excludeMembershipIndicator;
  }

/**
 * @return the uniqueMemberId
 */
public String getUniqueMemberId() {
	return uniqueMemberId;
}

/**
 * @param uniqueMemberId the uniqueMemberId to set
 */
public void setUniqueMemberId(String uniqueMemberId) {
	this.uniqueMemberId = uniqueMemberId;
}

public String getBussinessSectorCd() {
	return bussinessSectorCd;
}

public void setBussinessSectorCd(String bussinessSectorCd) {
	this.bussinessSectorCd = bussinessSectorCd;
}

   public List<String> getSeconddiagnosisCodesLists() {
	return seconddiagnosisCodesLists;
}

public void setSeconddiagnosisCodesLists(
		List<String> seconddiagnosisCodesLists) {
	this.seconddiagnosisCodesLists = seconddiagnosisCodesLists;
}

// set it for brms compat
public void setSeconddiagnosisCodesLists(
		ArrayList<String> seconddiagnosisCodesLists) {
	this.seconddiagnosisCodesLists = seconddiagnosisCodesLists;
}

public int getNumberOfDays() {
	return numberOfDays;
}

public void setNumberOfDays(int numberOfDays) {
	this.numberOfDays = numberOfDays;
}

public boolean isHistoryRequired() {
	return historyRequired;
}

public void setHistoryRequired(boolean historyRequired) {
	this.historyRequired = historyRequired;
}

public String getPrimaryDecisionCode() {
	return primaryDecisionCode;
}

public void setPrimaryDecisionCode(String primaryDecisionCode) {
	this.primaryDecisionCode = primaryDecisionCode;
}



	public String getSecondaryDiagnosisCode() {
		return secondaryDiagnosisCode;
	}

	public void setSecondaryDiagnosisCode(String secondaryDiagnosisCode) {
		this.secondaryDiagnosisCode = secondaryDiagnosisCode;
	}


   public ArrayList<DiagnosisCode> getDiagnosisCodesLists(){
      return diagnosisCodesLists;
   }

   public void setDiagnosisCodesLists(ArrayList<DiagnosisCode> diagnosisCodesLists)
   {
      this.diagnosisCodesLists = diagnosisCodesLists;
   }

   public String getDiagnosisCode() {
		return diagnosisCode;
	}
	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}

   public String getAdviceDecisionType() {
		return adviceDecisionType;
	}

	public void setAdviceDecisionType(String adviceDecisionType) {
		this.adviceDecisionType = adviceDecisionType;
	}

public String getPayAndEducate() {
		return payAndEducate;
	}

	public void setPayAndEducate(String payAndEducate) {
		this.payAndEducate = payAndEducate;
	}

public String getDxCanHaveSecondery()
   {
      return dxCanHaveSecondery;
   }

   public void setDxCanHaveSecondery(String dxCanHaveSecondery)
   {
      this.dxCanHaveSecondery = dxCanHaveSecondery;
   }
   public String getPatientDOB() {
		return patientDOB;
	}

	public void setPatientDOB(String patientDOB) {
		this.patientDOB = patientDOB;
	}

   public List<Integer> getPrimaryProcedureCodes()
   {
      return primaryProcedureCodes;
   }

   public void setPrimaryProcedureCodes(List<Integer> primaryProcedureCodes)
   {
      this.primaryProcedureCodes = primaryProcedureCodes;
   }

   public void setPrimaryProcedureCodes(ArrayList<Integer> primaryProcedureCodes)
   {
      this.primaryProcedureCodes = primaryProcedureCodes;
   }

   public String getError()
   {
      return error;
   }

   public void setError(String error)
   {
      this.error = error;
   }

   public java.lang.String getAssociatedMPolicy()
   {
      return associatedMPolicy;
   }

   public void setAssociatedMPolicy(java.lang.String associatedMPolicy)
   {
      this.associatedMPolicy = associatedMPolicy;
   }

   public java.lang.String getAssociatedMnc()
   {
      return associatedMnc;
   }

   public void setAssociatedMnc(java.lang.String associatedMnc)
   {
      this.associatedMnc = associatedMnc;
   }

   public java.lang.String getPrimaryProcedureCodeRequired()
   {
      return primaryProcedureCodeRequired;
   }

   public void setPrimaryProcedureCodeRequired(
         java.lang.String primaryProcedureCodeRequired)
   {
      this.primaryProcedureCodeRequired = primaryProcedureCodeRequired;
   }

   public String getDiagnosisCodes()
   {
      return DiagnosisCodes;
   }

   public void setDiagnosisCodes(String diagnosisCodes)
   {
      DiagnosisCodes = diagnosisCodes;
   }

   public String getPrimaryDiagnosisCode()
   {
      return primaryDiagnosisCode;
   }

   public void setPrimaryDiagnosisCode(String primaryDiagnosisCode)
   {
      this.primaryDiagnosisCode = primaryDiagnosisCode;
   }

   public List<String> getDiagnosisCodesList()
   {
      return diagnosisCodesList;
   }

   public void setDiagnosisCodesList(List<String> diagnosisCodesList)
   {
      this.diagnosisCodesList = diagnosisCodesList;
   }



   public String getPlaceOfService()
   {
      return placeOfService;
   }

   public void setPlaceOfService(String placeOfService)
   {
      this.placeOfService = placeOfService;
   }

   public int getCalculatedAge()
   {
      return calculatedAge;
   }

   public void setCalculatedAge(int calculatedAge)
   {
      this.calculatedAge = calculatedAge;
   }

   public String getPatientGenderCode()
   {
      return patientGenderCode;
   }

   public void setPatientGenderCode(String patientGenderCode)
   {
      this.patientGenderCode = patientGenderCode;
   }

   //private java.util.List<com.avalon.claim.workflow.module.ServiceLine> servicelines;

   public ClaimRequest()
   {
   }

   public java.lang.String getProcedureCode()
   {
      return this.procedureCode;
   }

   public void setProcedureCode(java.lang.String procedureCode)
   {
      this.procedureCode = procedureCode;
   }

   public java.lang.String getHealthPlanId()
   {
      return this.healthPlanId;
   }

   public void setHealthPlanId(java.lang.String healthPlanId)
   {
      this.healthPlanId = healthPlanId;
   }

   public java.util.Date getFromDateOfService()
   {
      return this.fromDateOfService;
   }

   public void setFromDateOfService(java.util.Date fromDateOfService)
   {
      this.fromDateOfService = fromDateOfService;
   }

   public java.util.Date getEffectiveEndDate()
   {
      return this.effectiveEndDate;
   }

   public void setEffectiveEndDate(java.util.Date effectiveEndDate)
   {
      this.effectiveEndDate = effectiveEndDate;
   }

   public java.lang.String getDecision()
   {
      return this.decision;
   }

   public void setDecision(java.lang.String decision)
   {
      this.decision = decision;
   }

   public java.lang.String getHealthPlanGroupId()
   {
      return this.healthPlanGroupId;
   }

   public void setHealthPlanGroupId(java.lang.String healthPlanGroupId)
   {
      this.healthPlanGroupId = healthPlanGroupId;
   }

   public java.lang.String getRenderingProviderNpi()
   {
      return this.renderingProviderNpi;
   }

   public void setRenderingProviderNpi(java.lang.String renderingProviderNpi)
   {
      this.renderingProviderNpi = renderingProviderNpi;
   }

   public java.lang.String getBillingProviderNpi()
   {
      return this.billingProviderNpi;
   }

   public void setBillingProviderNpi(java.lang.String billingProviderNpi)
   {
      this.billingProviderNpi = billingProviderNpi;
   }

   public java.lang.String getIdCardNumber()
   {
      return this.idCardNumber;
   }

   public void setIdCardNumber(java.lang.String idCardNumber)
   {
      this.idCardNumber = idCardNumber;
   }

   public java.lang.String getLineOfBusiness()
   {
      return this.lineOfBusiness;
   }

   public void setLineOfBusiness(java.lang.String lineOfBusiness)
   {
      this.lineOfBusiness = lineOfBusiness;
   }

   public java.lang.String getBlueCardCode()
   {
      return this.blueCardCode;
   }

   public void setBlueCardCode(java.lang.String blueCardCode)
   {
      this.blueCardCode = blueCardCode;
   }

   public java.lang.String getInNetworkIndicator()
   {
      return this.inNetworkIndicator;
   }

   public void setInNetworkIndicator(java.lang.String inNetworkIndicator)
   {
      this.inNetworkIndicator = inNetworkIndicator;
   }


   public java.lang.String getOtherClaimEditorIndicator()
   {
      return this.otherClaimEditorIndicator;
   }

   public void setOtherClaimEditorIndicator(
         java.lang.String otherClaimEditorIndicator)
   {
      this.otherClaimEditorIndicator = otherClaimEditorIndicator;
   }
   public java.lang.Integer getDiagnosisCodePointers()
   {
      return this.diagnosisCodePointers;
   }

   public void setDiagnosisCodePointers(java.lang.Integer diagnosisCodePointers)
   {
      this.diagnosisCodePointers = diagnosisCodePointers;
   }
   public int getUnits()
   {
      return units;
   }

   public void setUnits(int units)
   {
      this.units = units;
   }

   public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	
   public String getPaidDeniedIndicator() {
		return paidDeniedIndicator;
	}

	public void setPaidDeniedIndicator(String paidDeniedIndicator) {
		this.paidDeniedIndicator = paidDeniedIndicator;
	}

public boolean isSerivceLineDiagonsisCodesNotIn(List<String> diagnosisCodeLists,String rulesParam){
	   LOGGER.info("Start of sublistCode method calling" );
	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
	   boolean flag = true;
	   Iterator<String> itr = diagnosisCodeLists.iterator();
	      while(itr.hasNext()) {
	          String diagnosisCode = itr.next();
	          if(rulesList.contains(diagnosisCode)){
	        	  flag = false;

	          }

	      }
	   LOGGER.info("ruleslist flag" +flag);

	   return flag;
   }

   public boolean isSerivceLineDiagonsisCodesIn(List<String> diagnosisCodeLists,String rulesParam){
	   LOGGER.info("Start of sublistMatchCode method calling" );
	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
	   boolean flag = false;
	   Iterator<String> itr = diagnosisCodeLists.iterator();
	      while(itr.hasNext()) {
	          String diagnosisCode = itr.next();
	          if(rulesList.contains(diagnosisCode)){
	        	  flag = true;

	          }
	      }
	   LOGGER.info("flag-->" +flag);
	   return flag;
   }

   public Map buildunitsMap(Map unitMap){

	   return unitMap;
   }

   public boolean isAgeBetween(int calculatedAge,int frmAge, int tAge){
	   boolean flag=false;
	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge);
	   if(calculatedAge > frmAge && calculatedAge < tAge){
		   flag = true;
	   }
	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge+ "flag" + flag);
	   return flag;
   }

   public boolean isUnitDayswithinRange(Map<Integer, Integer> claimMap,int units,int days){
	   boolean flag = true;
	   if(claimMap.containsKey(days))
	   {
		   if(claimMap.get(days) > units)
		   {
			   flag = true;
		   }
	   }
	   return flag;
   }

   public boolean isPrimaryDCodeIn(String pDcode,String rulesParam){
	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
	   boolean flag = false;
	   if(rulesList.contains(pDcode)){
    	  flag = true;
	   }
	   return flag;
   }

@Override
public String toString() {
	return "ClaimRequest [procedureCode=" + procedureCode + ", healthPlanId="
			+ healthPlanId + ", fromDateOfService=" + fromDateOfService
			+ ", effectiveEndDate=" + effectiveEndDate
			+ ", primaryDiagnosisCode=" + primaryDiagnosisCode
			+ ", DiagnosisCodes=" + DiagnosisCodes + ", error=" + error
			+ ", calculatedAge=" + calculatedAge + ", associatedMPolicy="
			+ associatedMPolicy + ", associatedMnc=" + associatedMnc
			+ ", primaryProcedureCodeRequired=" + primaryProcedureCodeRequired
			+ ", diagnosisCodesList=" + diagnosisCodesList
			+ ", primaryProcedureCodes=" + primaryProcedureCodes
			+ ", decision=" + decision + ", patientDOB=" + patientDOB
			+ ", healthPlanGroupId=" + healthPlanGroupId
			+ ", renderingProviderNpi=" + renderingProviderNpi
			+ ", billingProviderNpi=" + billingProviderNpi
			+ ", lineOfBusiness=" + lineOfBusiness + ", blueCardCode="
			+ blueCardCode + ", idCardNumber=" + idCardNumber
			+ ", inNetworkIndicator=" + inNetworkIndicator
			+ ", primaryDecisionCode=" + primaryDecisionCode
			+ ", adviceDecisionType=" + adviceDecisionType + ", payAndEducate="
			+ payAndEducate + ", otherClaimEditorIndicator="
			+ otherClaimEditorIndicator + ", diagnosisCodePointers="
			+ diagnosisCodePointers + ", dxCanHaveSecondery="
			+ dxCanHaveSecondery + ", diagnosisCodesLists="
			+ diagnosisCodesLists + ", seconddiagnosisCodesLists="
			+ seconddiagnosisCodesLists + ", patientGenderCode="
			+ patientGenderCode + ", placeOfService=" + placeOfService
			+ ", diagnosisCode=" + diagnosisCode + ", units=" + units
			+ ", numberOfDays=" + numberOfDays + ", historyRequired="
			+ historyRequired + ", rank=" + rank + ", secondaryDiagnosisCode="
			+ secondaryDiagnosisCode + ", paidDeniedIndicator="
			+ paidDeniedIndicator + ", uniqueMemberId=" + uniqueMemberId
			+ ", bussinessSectorCd=" + bussinessSectorCd + "]";
}

}