package com.avalonhc.as.facts;

import java.util.ArrayList;
import java.util.List;

import com.avalon.cds.claim.historical.service.client.HistoricalClaimServiceClient;
import com.lbm.cds.claim.historical.sdo.ClaimLineData;
import com.lbm.cds.claim.historical.sdo.HistoricalClaimsResponse;
import java.util.logging.Logger; 

public class InvokeClaimHistoryClient {
	private final static Logger LOGGER = Logger.getLogger(InvokeClaimHistoryClient.class.getName());  

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LOGGER.info("Entering into main method");
		getHistroricalProcedureCodes("85236");
		LOGGER.info("End into main method");
	}
	public static ArrayList<String>  getHistroricalProcedureCodes(String procedureCode){
		String IdcardNumber = "SRS053910029760";
		String uniqueMemberId = "SRS053910029760";
		ArrayList<String> procedureCodeList = new ArrayList<String>();
		LOGGER.info("Entering into  start method11111");
		HistoricalClaimServiceClient historicalClaimServiceClient = new HistoricalClaimServiceClient();
		LOGGER.info(":::Entering into  call111111 ::::");
		HistoricalClaimsResponse historicalClaimsResponse = historicalClaimServiceClient.callClient(procedureCode, uniqueMemberId);
		LOGGER.info("After calling service"+historicalClaimsResponse);
		List<ClaimLineData> claimLineDataList = historicalClaimsResponse
				.getHistoricalClaims().getClaim().get(0).getLines().getLine();

		for (int i = 0; i < claimLineDataList.size(); i++) {
			System.out
					.println("claimLineDataList.get(i).getProcedureCode()::::"
							+ claimLineDataList.get(i).getProcedureCode());
			LOGGER.info("the value of i is ::::" + i);
			procedureCodeList.add(claimLineDataList.get(i).getProcedureCode());
			LOGGER.info("procedureCodeList is " + procedureCodeList);
		}
		LOGGER.info("Final procedureCodeList is " + procedureCodeList);
		
		return procedureCodeList;
	}
}