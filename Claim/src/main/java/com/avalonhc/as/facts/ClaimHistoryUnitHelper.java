//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.02.19 at 12:28:43 PM EST 
//


package com.avalonhc.as.facts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.logging.Logger; 


/**
 * <p>Java class for ClaimHistoryUnitHelper complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimHistoryUnitHelper">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimHistoryUnitHelper")
public class ClaimHistoryUnitHelper implements java.io.Serializable {
	private final static Logger LOGGER = Logger.getLogger(ClaimHistoryUnitHelper.class.getName());  

    public boolean isSerivceLineDiagonsisCodesIn(List<String> diagnosisCodeLists,String rulesParam){
   	   LOGGER.info("Start of sublistMatchCode method calling" );
   	   List<String> rulesList = new ArrayList<String>(Arrays.asList(rulesParam.split(",")));
   	   boolean flag = false;
   	   Iterator<String> itr = diagnosisCodeLists.iterator();
   	      while(itr.hasNext()) {
   	          String diagnosisCode = itr.next();
   	          if(rulesList.contains(diagnosisCode)){
   	        	  flag = true;
   	        	  
   	          }
   	      }
   	   LOGGER.info("ruleslist" +rulesList+"  diagnosisCodeLists" +diagnosisCodeLists);
   	   LOGGER.info("flag-->" +flag);
   	   return flag;
      }
      
      public Map<Integer, Integer> buildunitsMap(){
    	  Map<Integer, Integer> unitMap = new HashMap<Integer, Integer>();
    	  unitMap.put(1, 1);
    	  unitMap.put(30, 4);
    	  unitMap.put(540, 10);
    	  return unitMap;
      }

      public boolean isAgeBetween(int calculatedAge,int frmAge, int tAge){
   	   boolean flag=false;
   	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge);
   	   if(calculatedAge > frmAge && calculatedAge < tAge){
   		   flag = true;
   	   }
   	   LOGGER.info("Age Between" +" from : "+frmAge+" to :" +tAge +"calculatedAge :"+calculatedAge+ "flag" + flag);
   	   return flag;
      } 
      
      public boolean isUnitDayswithinRange(Map<Integer, Integer> claimMap,int units,int days){
   	   boolean flag = true;
   	   if(claimMap.containsKey(days))
   	   {
   		   if(claimMap.get(days) > units)
   		   {
   			   flag = true;
   		   }
   	   }
   	   return flag;
      }

      public boolean isUnitDayswithinRange(Map<Integer, Integer> claimMap,int claimUnits, int days, int unitperday){
      	   boolean flag = false;
      	   if(claimMap.containsKey(days))
      	   {
      		   int totalUnit = claimMap.get(days).intValue() + claimUnits;
      		   if(totalUnit >= unitperday)
      		   {
      			   flag = true;
      			   //LOGGER.info(" inside the helper :" +flag);
      		   }
      	   }
      	   return flag;
         }

}
