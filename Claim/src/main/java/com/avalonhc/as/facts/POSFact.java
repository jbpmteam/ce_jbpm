package com.avalonhc.as.facts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for POSFact complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="POSFact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="procedureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthPlanId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fromDateOfService" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="associatedMPolicy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="associatedMnc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diagnosisCodesList" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="placeOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */

@XmlType(name = "POSFact", propOrder = {
    "procedureCode",
    "healthPlanId",
    "fromDateOfService",
    "associatedMPolicy",
    "associatedMnc",
    "diagnosisCodesList",
    "decision",
    "placeOfService",
    "ListOfSecondaryCodes"
	

})
@XmlRootElement(name = "POSFact")

public class POSFact implements java.io.Serializable
{

   static final long serialVersionUID = 1L;
    private java.lang.String procedureCode;
   private java.lang.String healthPlanId;
   private java.util.Date fromDateOfService;
   private java.lang.String decision;
   private java.lang.String associatedMPolicy;
   private java.lang.String associatedMnc;
   private List<String> diagnosisCodesList = new ArrayList<String>();
   private ArrayList<SecondaryCodes> ListOfSecondaryCodes = new ArrayList<SecondaryCodes>();
   private String placeOfService;
public java.lang.String getProcedureCode() {
	return procedureCode;
}
public void setProcedureCode(java.lang.String procedureCode) {
	this.procedureCode = procedureCode;
}
public java.lang.String getHealthPlanId() {
	return healthPlanId;
}
public void setHealthPlanId(java.lang.String healthPlanId) {
	this.healthPlanId = healthPlanId;
}
public java.util.Date getFromDateOfService() {
	return fromDateOfService;
}
public void setFromDateOfService(java.util.Date fromDateOfService) {
	this.fromDateOfService = fromDateOfService;
}
public java.lang.String getDecision() {
	return decision;
}
public void setDecision(java.lang.String decision) {
	this.decision = decision;
}
public java.lang.String getAssociatedMPolicy() {
	return associatedMPolicy;
}
public void setAssociatedMPolicy(java.lang.String associatedMPolicy) {
	this.associatedMPolicy = associatedMPolicy;
}
public java.lang.String getAssociatedMnc() {
	return associatedMnc;
}
public void setAssociatedMnc(java.lang.String associatedMnc) {
	this.associatedMnc = associatedMnc;
}
public List<String> getDiagnosisCodesList() {
	return diagnosisCodesList;
}
public void setDiagnosisCodesList(List<String> diagnosisCodesList) {
	this.diagnosisCodesList = diagnosisCodesList;
}
public String getPlaceOfService() {
	return placeOfService;
}
public void setPlaceOfService(String placeOfService) {
	this.placeOfService = placeOfService;
}
public ArrayList<SecondaryCodes> getListOfSecondaryCodes() {
	return ListOfSecondaryCodes;
}
public void setListOfSecondaryCodes(
		ArrayList<SecondaryCodes> listOfSecondaryCodes) {
	ListOfSecondaryCodes = listOfSecondaryCodes;
}
  
     
}