package com.avalonhc.as.facts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.avalon.cds.claim.historical.service.client.HistoricalClaimServiceClient;
import com.lbm.cds.claim.historical.sdo.ClaimLineData;
import com.lbm.cds.claim.historical.sdo.HistoricalClaim;
import com.lbm.cds.claim.historical.sdo.HistoricalClaimsResponse;
import java.util.logging.Logger; 

public class ClaimHistoryClient {
	private final static Logger LOGGER = Logger.getLogger(ClaimHistoryClient.class.getName());  
	
	private HistoricalClaimsResponse historicalClaimsResponse;
	private ClaimRequest claimRequest;
	
	public ClaimRequest getClaimRequest() {
		return claimRequest;
	}

	public void setClaimRequest(ClaimRequest claimRequest) {
		this.claimRequest = claimRequest;
	}

	public HistoricalClaimsResponse clientCall(String procedureCode, String uniqueID, int  numberOfDays,Date toDate, String businessSectorId) throws ParseException {
		LOGGER.info("Inside the calling the clientCall method procedureCode "+procedureCode+ "  uniqueID  "+uniqueID +"  numberOfDays  "+numberOfDays+" toDate "+toDate+"  businessSectorId "+businessSectorId);
		   
		Date fromDateOfService =calculateFromDateOfServiceForClaimHistroyWS(toDate,numberOfDays);
		LOGGER.info("Calculated From date of service for claim histroy call is "+fromDateOfService);
		XMLGregorianCalendar xmlGregorianCalendarFromdate = toXMLGregorianCalendar(fromDateOfService);
		XMLGregorianCalendar xmlGregorianCalendarToDate = toXMLGregorianCalendar(toDate);
		LOGGER.info("Before calling the stub call xmlGregorianCalendarFromdate::"+xmlGregorianCalendarFromdate +" xmlGregorianCalendarToDate "+xmlGregorianCalendarToDate);
		HistoricalClaimServiceClient historicalClaimServiceClient = new HistoricalClaimServiceClient();
		HistoricalClaimsResponse historicalClaimsResponse = historicalClaimServiceClient.callClient(procedureCode, uniqueID, xmlGregorianCalendarFromdate,xmlGregorianCalendarToDate, businessSectorId);
		LOGGER.info("After  calling the stub call"+historicalClaimsResponse);
		return historicalClaimsResponse;
	}
	
	public int buildDosUnit(String uniqueID, String ProcedureCode, Date date,String bussinessSectorCd,HistoricalClaimsResponse historicalClaimsResponse,String requestCobIndicator){
		int unitDos = 0;
		XMLGregorianCalendar xmlGregorianCalendar = toXMLGregorianCalendar(date);
        
		LOGGER.info(":::Entering into  call111111 ::::"+historicalClaimsResponse);
		LOGGER.info("Claim Request cobIndicator passed into ClaimHistoryClient.buildDosUnit method: " + requestCobIndicator);

		if(null != historicalClaimsResponse.getHistoricalClaims()){
		
		Iterator<HistoricalClaim> histClaim = historicalClaimsResponse.getHistoricalClaims().getClaim().iterator();
	      while(histClaim.hasNext()) {
	          HistoricalClaim historicalClaim = histClaim.next();
	    	   Iterator<ClaimLineData> claimLineData = historicalClaim.getLines().getLine().iterator();
	    	   while(claimLineData.hasNext()){
	    		   ClaimLineData claimLnData = claimLineData.next();
    			   LOGGER.info(" inside : "+ claimLnData.getUnits().intValue() + " claim date : " 
	    		   +claimLnData.getToDateOfService() + " passed value : " + xmlGregorianCalendar);
    			   
	    		   if(claimLnData.getToDateOfService().equals((xmlGregorianCalendar)) || 
	    				   claimLnData.getToDateOfService() == (xmlGregorianCalendar)){
	    			   LOGGER.info(" inside : "+ claimLnData.getUnits().intValue());
	    			   //Add check for cobIndicator 
	    			   if (historicalClaim.getHeader().getCobIndicator().equals(requestCobIndicator)){
	    			     unitDos = unitDos + claimLnData.getUnits().intValue(); 
	    			   }
	    		   }
	    	   }
	      }
	}
      //  LOGGER.info(" unitDOS" + unitDos);
		return unitDos;
	}

	public Map<Integer, Integer> buildHistUnitMap(String uniqueID, String ProcedureCode, Date date,String bussinessSectorCd,HistoricalClaimsResponse historicalClaimsResponse ){
		XMLGregorianCalendar xmlGregorianCalendar = toXMLGregorianCalendar(date);
		XMLGregorianCalendar clmLatestDos = null;
		XMLGregorianCalendar todayDate = toXMLGregorianCalendar(new Date());
		
		Map<Integer, Integer> unitHist = new HashMap<Integer, Integer>();
		int units = 0;
		int dosunits = 0;
		int valueperday =0;
        
		LOGGER.info(":::Entering into  call22222 ::::");
		LOGGER.info("After calling service with out this object--->" + historicalClaimsResponse);
    
        /* expectation is claims are sorted based on DOS if not we need to add a logic to sort */
        
	if(null != historicalClaimsResponse.getHistoricalClaims()){
		Iterator<HistoricalClaim> histClaim = historicalClaimsResponse.getHistoricalClaims().getClaim().iterator();
	      while(histClaim.hasNext()) {
	          HistoricalClaim historicalClaim = histClaim.next();
	    	   Iterator<ClaimLineData> claimLineData = historicalClaim.getLines().getLine().iterator();
	    	   while(claimLineData.hasNext()){
	    		   ClaimLineData claimLnData = claimLineData.next();
    			   LOGGER.info(" inside : "+ claimLnData.getUnits().intValue() + " claim date : " 
	    		   +claimLnData.getToDateOfService() + " passed value : " + xmlGregorianCalendar);
    			   if (clmLatestDos==null ){
    				   clmLatestDos= claimLnData.getToDateOfService();
    			   }
    			   if (clmLatestDos.compare(claimLnData.getToDateOfService()) == DatatypeConstants.LESSER ){
    				   clmLatestDos= claimLnData.getToDateOfService();
    			   }
    			   clmLatestDos = claimLnData.getToDateOfService();
    			   if (unitHist.containsKey(numberOfDays(todayDate,claimLnData.getToDateOfService()))){
    				   dosunits = unitHist.get(numberOfDays(todayDate,claimLnData.getToDateOfService()));
    				   dosunits = dosunits+ claimLnData.getUnits().intValue();
        			   unitHist.put(numberOfDays(todayDate,claimLnData.getToDateOfService()), dosunits);
    			   }
    			   else
    			   {
    				   dosunits = claimLnData.getUnits().intValue();
    				   unitHist.put(numberOfDays(todayDate,claimLnData.getToDateOfService()), dosunits);
    			   }
	    	   }
	      }
	      /* add the units incremental of days*/
			Iterator<Entry<Integer,Integer>> iterator = unitHist.entrySet().iterator();
			while (iterator.hasNext()) {
			Entry<Integer, Integer> entry = (Map.Entry<Integer,Integer>) iterator.next();
			units = units + entry.getValue();
			unitHist.put(entry.getKey(), units);
			}



	      for(int i=0;i<6000; i++){
			if(unitHist.containsKey(i))
			{
				valueperday= (Integer) unitHist.get(i);
			}
			else
			{
				unitHist.put(i, valueperday);
			}
        }
	}
      // LOGGER.info("buildHistUnitMap::::"+ clmLatestDos + " ::::"+ unitHist);
	return unitHist;
	}

	public Map<Integer, Integer> addHistUnitMap(Map<Integer, Integer> histMap, Date frmDateofService, int addUnit){
		  LOGGER.info("Inside the addHistUnitMap method histMap"+histMap +"addUnit"+addUnit);
		Map<Integer, Integer> addHistMap = new HashMap<Integer,Integer>();
		XMLGregorianCalendar todayDate = toXMLGregorianCalendar(new Date());
		 LOGGER.info("Inside the addHistUnitMap method  frmDateofService"+frmDateofService);
		int startDay = numberOfDays(todayDate ,toXMLGregorianCalendar(frmDateofService));
		 LOGGER.info("Inside the addHistUnitMap method  startDay"+startDay);
		
		 if (! histMap.isEmpty()){
		for(int i=0;i<6000; i++){
			 
			if(i>startDay){
				int newValue = (Integer) histMap.get(i) + addUnit;
				addHistMap.put(i, newValue);
			}
			else{
				addHistMap.put(i, histMap.get(i));
			}
        }
		}
		return addHistMap;
	}

	public static XMLGregorianCalendar stringToXMLGregorianCalendar(String s)	
	{
		Date date;
		SimpleDateFormat simpleDateFormat;
		GregorianCalendar gregorianCalendar;
		simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar cal = new GregorianCalendar();
		XMLGregorianCalendar xmlDate = null;
		try {
			date = simpleDateFormat.parse(s);
			gregorianCalendar =
			(GregorianCalendar) GregorianCalendar.getInstance();
			gregorianCalendar.setTime(date);
			cal.setTime(date);
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		
		return xmlDate;

	}
	
    public static XMLGregorianCalendar toXMLGregorianCalendar(Date date){

        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
		GregorianCalendar cal = new GregorianCalendar();
        try {
//            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
			cal.setTime(date);
			xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);

        } catch (Exception e) {
        	e.printStackTrace();
        }

        return xmlCalendar;

    }

    

    public static Date toDate(XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();

    }

    public static int numberOfDays(XMLGregorianCalendar xmlGregorianCalendar, XMLGregorianCalendar clmLatestDos){
    	int noofdays = (int) ((toDate(xmlGregorianCalendar).getTime()- toDate(clmLatestDos).getTime())/ (1000 * 60 * 60 * 24));
    	return noofdays;
    }
    
   

	/**
	 * @return the historicalClaimsResponse
	 */
	public HistoricalClaimsResponse getHistoricalClaimsResponse() {
		return historicalClaimsResponse;
	}

	
	public void setHistoricalClaimsResponse(String ProcedureCode, String uniqueID, XMLGregorianCalendar fromDate, XMLGregorianCalendar toDate, String businessSectorCD) {
		HistoricalClaimServiceClient historicalClaimServiceClient = new HistoricalClaimServiceClient();
		this.historicalClaimsResponse = historicalClaimServiceClient.callClient(ProcedureCode,uniqueID,fromDate, toDate, businessSectorCD);;
	}
	
	
	public  ArrayList<String> getHistroricalProcedureCodes(String procedureCode, String uniqueID, XMLGregorianCalendar fromDate,XMLGregorianCalendar toDate, String businessSectorId) {
		ArrayList<String> procedureCodeList = new ArrayList<String>();
		LOGGER.info("Entering into  start method12221");
		HistoricalClaimServiceClient historicalClaimServiceClient = new HistoricalClaimServiceClient();
		
		LOGGER.info(":::Entering into  call3333 ::::");
		HistoricalClaimsResponse historicalClaimsResponse = historicalClaimServiceClient.callClient(procedureCode, uniqueID, fromDate, toDate, businessSectorId);
		LOGGER.info("After calling service with out this object--->" + historicalClaimsResponse);
		LOGGER.info("buildDosUnit ::CHeck the value of  this.getHistoricalClaimsResponse()--->" + this.getHistoricalClaimsResponse());
		
	if(null != historicalClaimsResponse.getHistoricalClaims()){		
		List<ClaimLineData> claimLineDataList = historicalClaimsResponse.getHistoricalClaims().getClaim().get(0).getLines().getLine();

		for (int i = 0; i < claimLineDataList.size(); i++) {
			LOGGER.info("claimLineDataList.get(i).getProcedureCode()::::"
							+ claimLineDataList.get(i).getProcedureCode());
			LOGGER.info("the value of i is ::::" + i);
			procedureCodeList.add(claimLineDataList.get(i).getProcedureCode());
			LOGGER.info("procedureCodeList is " + procedureCodeList);
		}
		LOGGER.info("Final procedureCodeList is " + procedureCodeList);
	}
		return procedureCodeList;
	}
	
	public  ArrayList<String> getHistroricalProcedureCode(HistoricalClaimsResponse historicalClaimsResponse) {
		ArrayList<String> procedureCodeList = new ArrayList<String>();
		LOGGER.info("Entering into  start method11111");
   	if(null != historicalClaimsResponse.getHistoricalClaims()){		
		List<ClaimLineData> claimLineDataList = historicalClaimsResponse.getHistoricalClaims().getClaim().get(0).getLines().getLine();

		for (int i = 0; i < claimLineDataList.size(); i++) {
			LOGGER.info("claimLineDataList.get(i).getProcedureCode()::::"
							+ claimLineDataList.get(i).getProcedureCode());
			LOGGER.info("the value of i is ::::" + i);
			procedureCodeList.add(claimLineDataList.get(i).getProcedureCode());
			LOGGER.info("procedureCodeList is " + procedureCodeList);
		}
		LOGGER.info("Final procedureCodeList is " + procedureCodeList);
	}
		return procedureCodeList;
	}
    
    public   Date  calculateFromDateOfServiceForClaimHistroyWS(Date toDateOfService, int numberOfDays) throws ParseException{
   	     LOGGER.info("toDateOfService--"+toDateOfService +"::numberOfDays::"+numberOfDays);
    	
   	     Date fromDateOfService = null ;
         final java.util.Calendar cal = GregorianCalendar.getInstance();
         cal.setTime( toDateOfService );
         cal.add( GregorianCalendar.DATE,-numberOfDays); // date manipulation
         fromDateOfService = cal.getTime() ;
         LOGGER.info( "result: " + fromDateOfService ); 
    	
         return fromDateOfService;
    }
    public boolean CheckAvailbilityForPxCompatability( ClaimRequest claimReq,String rulesParam){
    	LOGGER.info("Inside CheckAvailbilityForPrimaryRequired method ");
    	LOGGER.info("ClaimRequest--"+claimReq);
    	LOGGER.info("rulesParam---"+rulesParam);
 	   boolean flag;
 	   ArrayList<String> primaryProcedureCodeList=new ArrayList<String>();
 	   LOGGER.info("claimReq.getFromDateOfService() date ---"+claimReq.getFromDateOfService());
 	  XMLGregorianCalendar xmlGregorianCalendarFromdate = toXMLGregorianCalendar(claimReq.getFromDateOfService());
 	  LOGGER.info("xmlGregorianCalendarFromdate date ---"+xmlGregorianCalendarFromdate);
 	   LOGGER.info(" CheckAvailbilityForPrimaryRequired::: procedureCode "+claimReq.getProcedureCode()+"uniqueMemberId " +claimReq.getUniqueMemberId() +":::bussinessSectorCd::" +claimReq.getBussinessSectorCd());
 	   primaryProcedureCodeList = getHistroricalProcedureCodes(claimReq.getProcedureCode(),claimReq.getUniqueMemberId(), xmlGregorianCalendarFromdate,xmlGregorianCalendarFromdate,claimReq.getBussinessSectorCd());
 	  ClaimRequest claimRequest =new ClaimRequest();  
 	   flag= claimRequest.isSerivceLineDiagonsisCodesIn(primaryProcedureCodeList,rulesParam);

 	   LOGGER.info(" CheckAvailbilityForPrimaryRequired::: diagnosisCodeLists" +primaryProcedureCodeList+"::Final flag-->" +flag);
 	   return flag;
    } 
    
}
