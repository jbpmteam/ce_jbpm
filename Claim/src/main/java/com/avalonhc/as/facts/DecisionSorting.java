package com.avalonhc.as.facts;   

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger; 
import com.avalonhc.as.facts.SecondaryCodes;

public class DecisionSorting implements Serializable {
	private final static Logger LOGGER = Logger.getLogger(DecisionSorting.class.getName());  
/*public static void main(String[] args) {
		
	HashMap hashMap = new HashMap<Integer, SecondaryCodes>();

	 SecondaryCodes secondaryCodes1=new SecondaryCodes();

     secondaryCodes1.setSecondaryDecisionCode("111");

     secondaryCodes1.setSecondaryPolicyTag("Policytag for E&I");

     secondaryCodes1.setSecondaryPolicyNecessityCriteria("Policy ncc");

     SecondaryCodes secondaryCodes2=new SecondaryCodes();

     secondaryCodes2.setSecondaryDecisionCode("112");

     secondaryCodes2.setSecondaryPolicyTag("Policag for E&I");

     secondaryCodes2.setSecondaryPolicyNecessityCriteria("Polcc.cre for E&I");
     
     hashMap.put(4000,secondaryCodes1);
     hashMap.put(14,secondaryCodes2);
     hashMap.put(111,secondaryCodes2);
     hashMap.put(200,secondaryCodes2);
     hashMap.put(1111,secondaryCodes2);
     
		LOGGER.info("before sorting");
		LOGGER.info(secondaryCodes1);
		LOGGER.info(secondaryCodes2);
		LOGGER.info("after sorting");
		TreeMap<Integer, SecondaryCodes> outputMap =sortMap(hashMap);
		LOGGER.info("outputMap--->"+outputMap);
		
		for(Map.Entry<Integer,SecondaryCodes> entry : outputMap.entrySet()) {
			  int key = entry.getKey();
			  SecondaryCodes value = entry.getValue();

			  LOGGER.info(key + " => " + value);
			}
		
		}*/
	
public static  TreeMap<Integer, SecondaryCodes> sortMap(HashMap<Integer, SecondaryCodes> inputMap){
	
	TreeMap<Integer, SecondaryCodes> map = new TreeMap<Integer, SecondaryCodes>(inputMap);
	Set set = map.entrySet();
	Iterator iterator = set.iterator();
	int key;
	SecondaryCodes value;
	while (iterator.hasNext()) {
		Map.Entry me = (Map.Entry) iterator.next();
		LOGGER.info(me.getKey() + ": "+me.getValue());
		key =(Integer)me.getKey();
		value =(SecondaryCodes)me.getValue();
		map.put(key, value);
	}
	return  map;
}

public static ResponseLineData   generateClaimResponse(TreeMap<Integer, SecondaryCodes> inputMap,ResponseLineData responseLineData){
	//ResponseLineData responseLineData =new ResponseLineData ();
	Set set = inputMap.entrySet();
	Iterator iterator = set.iterator();
	int key;
	SecondaryCodes secondaryCodes;
	int count =0;
	while (iterator.hasNext()) {
		Map.Entry me = (Map.Entry) iterator.next();
		LOGGER.info(me.getKey() + ": "+me.getValue());
		key =(Integer)me.getKey();
		secondaryCodes =(SecondaryCodes)me.getValue();
		LOGGER.info("Before setting response count::: "+count);
		if (count==0){
			responseLineData.setPrimaryDecisionCode(secondaryCodes.getSecondaryDecisionCode());
			LOGGER.info("responseLineData.getPrimaryDecisionCode()::::"+responseLineData.getPrimaryDecisionCode());
			
			responseLineData.setPrimaryPolicyNecessityCriterion(secondaryCodes.getSecondaryPolicyNecessityCriteria());
			LOGGER.info("secondaryCodes.getSecondaryPolicyNecessityCriteria()::::"+responseLineData.getPrimaryPolicyNecessityCriterion());
			responseLineData.setPrimaryPolicyTag(secondaryCodes.getSecondaryPolicyTag());
			LOGGER.info("secondaryCodes.getSecondaryPolicyTag()::::"+responseLineData.getPrimaryPolicyTag());
			/*responseLineData.setPayAndEducate("N");
			responseLineData.setAdviceDecisionType("00");*/
			
			
			//evaluateLabClaimResponse.getResponseLine().add(responseLineData);
			LOGGER.info("Count increased value is before :::::: "+count);
			count++;
			LOGGER.info("Count increased value is after:::::: "+count);
			
		}
		else {
			
			responseLineData.getSecondaryCodesData().add(secondaryCodes);
			LOGGER.info("Count increased value after secondaryCodes:::::: "+secondaryCodes);
			//evaluateLabClaimResponse.getResponseLine().add(responseLineData);
		}
		
		//set PayAndEducate to "N" if any secondary policy tag does not match with the primary one
		if ((responseLineData.getPrimaryPolicyTag()) != (secondaryCodes.getSecondaryPolicyTag()) &&
			    secondaryCodes.getSecondaryPolicyTag() != null){
				
				responseLineData.setPayAndEducate("N");
				
		}
		
		//Set the approve service unit count to zero if if any secondary decision code starts with D and ends with R
		//and pay And Educate flag is N
	    if (secondaryCodes.getSecondaryDecisionCode().startsWith("D") && secondaryCodes.getSecondaryDecisionCode().endsWith("R") &&
	    	responseLineData.getPayAndEducate().equals("N")){
		
			responseLineData.setApprovedServiceUnitCount(0);
		
	    }
		
	}
	//evaluateLabClaimResponse.getResponseLine().add(responseLineData);
	LOGGER.info("responseLineData value is :::::: "+responseLineData);
	return responseLineData;
}



}
