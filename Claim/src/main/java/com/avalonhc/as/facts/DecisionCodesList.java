package com.avalonhc.as.facts;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for DecisionCodesList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DecisionCodesList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policyNecessityCriteria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="decisionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecisionCodesList", propOrder = {
    "policyTag",
    "policyNecessityCriteria",
    "decisionCode"
})
@XmlRootElement(name = "decisionCodesList")
public class DecisionCodesList implements Serializable{

	private String policyTag;
	private String policyNecessityCriteria;
	private String decisionCode;

	public String getDecisionCode() {
		return decisionCode;
	}

	public void setDecisionCode(String decisionCode) {
		this.decisionCode = decisionCode;
	}
	
	public String getPolicyTag() {
		return policyTag;
	}

	public void setPolicyTag(String policyTag) {
		this.policyTag = policyTag;
	}
	
	public String getPolicyNecessityCriteria() {
		return policyNecessityCriteria;
	}

	public void setPolicyNecessityCriteria(String policyNecessityCriteria) {
		this.policyNecessityCriteria = policyNecessityCriteria;
	}
}
