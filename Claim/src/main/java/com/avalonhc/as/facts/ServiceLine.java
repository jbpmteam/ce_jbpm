package com.avalonhc.as.facts;

import java.util.Date;
import java.util.ArrayList;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class ServiceLine implements java.io.Serializable
{

   static final long serialVersionUID = 1L;
   //private java.lang.String associatedMPolicy;

 //  private java.lang.String associatedMnc;
   private java.lang.String inNetworkIndicator;
   //private String responseCode;
   private java.lang.String otherClaimEditorIndicator;
   private String procedureCode;
   private String fromDateOfService;
   private String effectiveEndDate;
   private String placeOfService;
   
   private ArrayList<DecisionCodesList> decisionCodesListResponse=new ArrayList<DecisionCodesList>();
   
   public ArrayList<DecisionCodesList> getDecisionCodesListResponse() {
 		return decisionCodesListResponse;
 	}
 	public void setDecisionCodesListResponse(ArrayList<DecisionCodesList> decisionCodesListResponse) {
 		this.decisionCodesListResponse = decisionCodesListResponse;
 	}

/*   public java.lang.String getAssociatedMPolicy()
   {
      return associatedMPolicy;
   }

   public void setAssociatedMPolicy(java.lang.String associatedMPolicy)
   {
      this.associatedMPolicy = associatedMPolicy;
   }*/

 /*  public java.lang.String getAssociatedMnc()
   {
      return associatedMnc;
   }

   public void setAssociatedMnc(java.lang.String associatedMnc)
   {
      this.associatedMnc = associatedMnc;
   }*/

   public ServiceLine()
   {
   }

   public String getPlaceOfService()
   {
      return placeOfService;
   }

   public void setPlaceOfService(String placeOfService)
   {
      this.placeOfService = placeOfService;
   }

  

   public String getProcedureCode()
   {
      return procedureCode;
   }

   public void setProcedureCode(String procedureCode)
   {
      this.procedureCode = procedureCode;
   }

   public String getFromDateOfService()
   {
      return fromDateOfService;
   }

   public void setFromDateOfService(String fromDateOfService)
   {
      this.fromDateOfService = fromDateOfService;
   }

   public String getEffectiveEndDate()
   {
      return effectiveEndDate;
   }

   public void setEffectiveEndDate(String effectiveEndDate)
   {
      this.effectiveEndDate = effectiveEndDate;
   }

   /**
    * @return the responseCode
    */
  /* public String getResponseCode()
   {
      return responseCode;
   }

   *//**
    * @param responseCode the responseCode to set
    *//*
   public void setResponseCode(String responseCode)
   {
      this.responseCode = responseCode;
   }
*/
   public java.lang.String getInNetworkIndicator()
   {
      return this.inNetworkIndicator;
   }

   public void setInNetworkIndicator(java.lang.String inNetworkIndicator)
   {
      this.inNetworkIndicator = inNetworkIndicator;
   }

   public java.lang.String getOtherClaimEditorIndicator()
   {
      return this.otherClaimEditorIndicator;
   }

   public void setOtherClaimEditorIndicator(
         java.lang.String otherClaimEditorIndicator)
   {
      this.otherClaimEditorIndicator = otherClaimEditorIndicator;
   }

/*   public ServiceLine(java.lang.String associatedMPolicy,
         java.lang.String associatedMnc, java.lang.String inNetworkIndicator,
         java.lang.String otherClaimEditorIndicator,
         java.lang.String procedureCode, java.lang.String fromDateOfService,
         java.lang.String effectiveEndDate, java.lang.String placeOfService,
         java.lang.String responseCode)
   {
      this.associatedMPolicy = associatedMPolicy;
      this.associatedMnc = associatedMnc;
      this.inNetworkIndicator = inNetworkIndicator;
      this.otherClaimEditorIndicator = otherClaimEditorIndicator;
      this.procedureCode = procedureCode;
      this.fromDateOfService = fromDateOfService;
      this.effectiveEndDate = effectiveEndDate;
      this.placeOfService = placeOfService;
      this.responseCode = responseCode;
   }*/

}